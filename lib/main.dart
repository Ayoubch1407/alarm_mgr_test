import 'dart:async';
import 'dart:isolate';
import 'dart:ui';
import 'package:android_alarm_manager/android_alarm_manager.dart';
import 'package:flutter/material.dart';
import 'package:geolocator/geolocator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:http/http.dart' as http;

//Test from Doculentation
const String countKey = 'count';
const String isolateName = 'isolate';
final ReceivePort port = ReceivePort();
late SharedPreferences prefs;


//--------------------------------
var locationMsg = "";
int c = 0;
Timer? timer;

//variables pour manipluler les buttons
bool startBtnStatus = true;
bool stopBtnStatus = false;


Future<void> main() async{
  WidgetsFlutterBinding.ensureInitialized();
  IsolateNameServer.registerPortWithName(
    port.sendPort,
    isolateName,
  );
  prefs = await SharedPreferences.getInstance();
  if (!prefs.containsKey(countKey)) {
    await prefs.setInt(countKey, 0);
  }
  runApp(MyApp());
  //await AndroidAlarmManager.initialize();
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(

      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int alarmId = 1;
  int _counter = 0;


  @override
  void initState() {
    super.initState();
    //AndroidAlarmManager.initialize();
    port.listen((_) async => await _incrementCounter());
  }

  Future<void> _incrementCounter() async {
    print('Increment counter!');
    await prefs.reload();

    setState(() {
      _counter++;
    });
  }

  static SendPort? uiSendPort;

  static Future<void> callback() async {
    timer?.cancel();
    print('Alarm fired!');
    final prefs = await SharedPreferences.getInstance();
    int currentCount = prefs.getInt(countKey) ?? 0;
    await prefs.setInt(countKey, currentCount + 1);
    uiSendPort ??= IsolateNameServer.lookupPortByName(isolateName);
    uiSendPort?.send(null);
    //Recuperer et envyer la localisation au serveur :
    final GeolocatorPlatform geolocatorPlatform = GeolocatorPlatform.instance;
    var position = await geolocatorPlatform.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    var lat = position.latitude;
    var long = position.longitude;
    var user = "Ayoub Fibonacci";
    var url = Uri.parse("https://pale-fetches.000webhostapp.com/insertdata.php");
    final response = await http.post(url, body: {
      "user": user,
      "lat": lat.toString(),
      "long": long.toString(),
    });
    //Repter la tache chaque 2 secondes
    timer = Timer(Duration(seconds: 2), () async{
      callback();
    });

  }

  void startBtnAction() async{
    AndroidAlarmManager.initialize();
    print("Sending btn cliked !");
    callback();
    await AndroidAlarmManager.periodic(
      const Duration(minutes: 1),
      // Ensure we have a unique alarm ID.
      alarmId,
      callback,
      exact: true,
      wakeup: true,
    );
    setState(() {
      startBtnStatus = false;
      stopBtnStatus = true;
    });
  }

  void stopBtnAction() async{
    print("Stop btn cliked !");
    AndroidAlarmManager.cancel(alarmId);
    timer?.cancel();
    await prefs.setInt(countKey, 0);
    uiSendPort ??= IsolateNameServer.lookupPortByName(isolateName);
    uiSendPort?.send(null);
    setState(() {
      startBtnStatus = true;
      stopBtnStatus = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    final textStyle = Theme
        .of(context)
        .textTheme
        .headline4;
    return Scaffold(
      appBar: AppBar(
        title: Text("AlarmManager Test"),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  'Total sending times: ',
                  style: textStyle,
                ),
                SizedBox(height: 5.0,),
                Text(
                  prefs.getInt(countKey).toString(),
                  key: ValueKey('BackgroundCountText'),
                  style: textStyle,
                ),
              ],
            ),
            ElevatedButton(
              child: Text(
                'Start sending to server',
              ),
              key: ValueKey('RegisterOneShotAlarm'),
              onPressed: startBtnStatus?startBtnAction: null,
            ),
            ElevatedButton(
                onPressed: stopBtnStatus?stopBtnAction: null,
                child: Text(
                  "Stop sending to server"
                ),
                style: ButtonStyle(backgroundColor: MaterialStateProperty.all<Color>(Colors.redAccent)),
            ),
          ],
        ),
      ),
    );
  }

}



